from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options

from time import sleep
from bs4 import BeautifulSoup

import atexit

class G_Driver(webdriver.Chrome):

    def __enter__(self):
        def exit_handler():
            self.quit()
        atexit.register(exit_handler)
        atexit.unregister(exit_handler)
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.quit()

    def __del__(self):
        self.quit()


chrome_options = Options()
chrome_options.add_argument("--headless")

g_driver = G_Driver(options=chrome_options)

def Know_feeling_ar(text):
    
    url = "https://www.arabitools.com/sentest.html"
    g_driver.get(url)
    sleep(1.5)
    
    textarea = g_driver.find_element(By.TAG_NAME, "textarea")
    btn_cc = g_driver.find_element(By.ID, "cc")
    data = g_driver.find_element(By.ID, "data")
    
    sleep(1.5)
    textarea.clear()
    textarea.send_keys(text)
    btn_cc.click()
    sleep(1.5)
    n = 0
    while True:
        if n == 10:
            pass

        tmp = data.text
        if tmp == 'جاري تحليل النص ...':
            sleep(1.5)
            n+=1
            continue
        break
    tmp_sip = tmp.split('،')
    if len(tmp_sip) == 2:
        type_ = tmp_sip[0].replace(" ", "").replace("النص:", "")
        present = tmp_sip[1].replace(" ", "").replace("بنسبة:", "").replace("%", "")
        present = float(present)
    elif len(tmp_sip) == 1:
        type_ = tmp_sip[0].replace(" ", "").replace("النص:", "")
        present = None

    return type_, present

def Know_lang_by_AI(text):
    
    url = f"https://translate.google.com/?sl=auto&text={text}&op=translate"
    g_driver.get(url)
    sleep(5)
    html = g_driver.page_source
    soup = BeautifulSoup(html, features="lxml")
    tag = soup.find("div", {'class': "ooArgc"})
    result_tmp = tag.text
    result_tmp = result_tmp.replace("- تم رصدها", "")
    result_tmp = result_tmp.replace(" - Detected", "")
    
    
    if result_tmp == 'الإنجليزية' or result_tmp == 'English':
        result = 'en'
    elif result_tmp == 'العربية' or result_tmp == 'Arabic':
        result = 'ar'
    else:
        result = result_tmp

    return result
